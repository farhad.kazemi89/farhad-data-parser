const qs = require('querystring');
const c = require('./config');

async function Parser(req, res, next) {
  try {
    if (req.method === 'GET') {
      req.data = getQueryStringData(req);
    }
    else {
      req.data = await getPostData(req);
    }

    next();
  }
  catch (e) {
    res.statusCode = c.statusCodes.INTERNAL_SERVER_ERROR;
    res.setHeader('Content-Type', c.contentTypes.JSON);
    console.log(`${c.errors.internalServerError.code}: ${c.errors.internalServerError.message}`);
    res.end(JSON.stringify(c.errors.internalServerError));
  }
}

function getQueryStringData(req) {
  let data = null;
  let queryString = null;
  try {
    queryString = req.url.split('?')[1];
    if (!queryString) {
      data = null;
    }
    else {
      data = qs.parse(queryString);
    }
  }
  catch (e) {
    data = queryString;
  }

  return data;
}

async function getPostData(req) {
  let data = null;
  return new Promise((resolve, reject) => {
    let buffer = '';
    req.on('data', chunk => {
      buffer += chunk;
    });

    req.on('end', () => {
      try {
        data = JSON.parse(buffer);
      }
      catch (e) {
        data = buffer;
      }

      resolve(data);
    })
  });
}

module.exports = Parser;